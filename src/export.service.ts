import {WriteStream} from 'fs';
import {Util} from './util';

export class ExportService {
    constructor(private firestore: FirebaseFirestore.Firestore) {}

    async export(
        path: string,
        writeStream: WriteStream,
        timestamp: string,
        transformer: (data: any, path: string) => Promise<any> = async (data) => data,
        excludedCollections: string[] = [],
    ): Promise<void> {
        await Util.safeWrite(writeStream, '{');

        const {isRoot, isDocument, isCol, ref} = Util.getRef(this.firestore, path);
        if (isRoot) {
            await this.processCollections(writeStream, transformer, excludedCollections, ref);
        } else if (isDocument) {
            await this.processDocument(writeStream, transformer, excludedCollections, await ref.get(), true);
        } else if (isCol) {
            await this.processCollection(writeStream, transformer, excludedCollections, ref);
        }

        await Util.safeWrite(writeStream, `"metadata": ${JSON.stringify({timestamp, ref: path})}`);
        await Util.safeWrite(writeStream, '}');
        console.log('Export complete');
    }

    private async processDocument(
        writeStream: WriteStream,
        transformer: (data: any, path: string) => Promise<any>,
        excludedCollections: string[] = [],
        snap: FirebaseFirestore.DocumentSnapshot,
        isStartOfProcess = false,
    ) {
        const transformedData = await transformer(snap.data(), snap.ref.path);
        if (!!transformedData) {
            const data = Util.serializeSpecialTypes(transformedData);
            await Util.safeWrite(writeStream, `"${snap.ref.path}": ${JSON.stringify(data)},`);
        }

        if (isStartOfProcess) {
            await this.processCollections(writeStream, transformer, excludedCollections, snap.ref);
        }
    }

    private async processCollections(
        writeStream: WriteStream,
        transformer: (data: any, path: string) => Promise<any>,
        excludedCollections: string[] = [],
        ref: FirebaseFirestore.DocumentReference | FirebaseFirestore.Firestore,
    ) {
        const collections = await ref.listCollections();
        for (let j = 0; j < collections.length; j++) {
            await this.processCollection(writeStream, transformer, excludedCollections, collections[j]);
        }
    }

    private async processCollection(
        writeStream: WriteStream,
        transformer: (data: any, path: string) => Promise<any>,
        excludedCollections: string[] = [],
        ref: FirebaseFirestore.CollectionReference,
    ) {
        const exclusion = excludedCollections.find((exclusion) => new RegExp(exclusion).test(ref.path));
        if (exclusion) {
            console.log(`Not processing export collection ${ref.path} due to exclusion ${exclusion}`);
            return;
        }
        console.log(`Processing export collection ${ref.path}`);

        let startDoc: any = undefined;
        const nestedCollections = [];

        let snap: FirebaseFirestore.QuerySnapshot;
        while (!snap || snap.size > 0) {
            let query = ref.limit(Util.batchSize);
            if (startDoc) {
                query = query.startAfter(startDoc);
            }

            snap = await query.get();
            if (snap.size > 0) {
                startDoc = snap.docs[snap.size - 1];
                console.log(`Export at ` + snap.docs[0].id);
            }

            await Util.batchExecutor(snap.docs.length, (batchSize) =>
                snap.docs.splice(0, batchSize).map(async (docSnapshot) => {
                    const collections = await docSnapshot.ref.listCollections();
                    if (collections.length > 0) {
                        nestedCollections.push(...collections);
                    }
                    await this.processDocument(writeStream, transformer, excludedCollections, docSnapshot);
                }),
            );
        }

        for (let j = 0; j < nestedCollections.length; j++) {
            await this.processCollection(writeStream, transformer, excludedCollections, nestedCollections[j]);
        }
    }
}
