import {FirebaseAdmin} from './firebase-admin';
import {ImportService} from './import.service';
import * as fs from 'fs';

export class FirestoreImport {
    constructor(
        firebaseAdmin: FirebaseAdmin,
        private importService: ImportService = new ImportService(firebaseAdmin.firestore),
    ) {}

    async execute(file: string): Promise<boolean> {
        console.log(`Reading data from ${process.cwd()}/${file}`);
        const readStream = fs.createReadStream(`${process.cwd()}/${file}`);

        try {
            await this.importService.import(readStream);
            console.log(`Import done`);
        } catch (e) {
            console.error('Could not export data', e);
            return false;
        }
        readStream.close();

        return true;
    }
}
