import {WriteStream} from 'fs';
import * as admin from 'firebase-admin';
import * as moment from 'moment';
import {ITimestamp} from './node-firestore-import-export/interfaces/ITimestamp';
import {IGeopoint} from './node-firestore-import-export/interfaces/IGeopoint';
import {IDocumentReference} from './node-firestore-import-export/interfaces/IDocumentReference';

export class Util {
    static readonly ROOT = '__ROOT__';
    static DEFAULT_BATCH_SIZE: number = 50;
    static batchSize: number = Util.DEFAULT_BATCH_SIZE;

    static getRef(firestore: FirebaseFirestore.Firestore, path: string): ComplexRef {
        if (path === Util.ROOT) {
            return {
                isRoot: true,
                isDocument: false,
                isCol: false,
                ref: firestore,
            };
        }

        let tempPath = path;
        tempPath = tempPath.startsWith('/') ? tempPath.substring(1, tempPath.length - 1) : tempPath;
        tempPath = tempPath.endsWith('/') ? tempPath.substring(0, tempPath.length - 2) : tempPath;
        const isDocument = tempPath.split('/').length % 2 === 0;

        return {
            isRoot: false,
            isDocument: isDocument,
            isCol: !isDocument,
            ref: isDocument ? firestore.doc(path) : firestore.collection(path),
        };
    }

    static safeWrite(writeStream: WriteStream, chunk: string): Promise<any> {
        return new Promise((resolve, reject) => {
            writeStream.write(chunk + '\n', (e) => (e ? reject(e) : resolve(chunk + '\n')));
        });
    }

    static async batchExecutor<T>(
        total: number,
        builder: (batchSize: number) => Promise<any>[],
        batchSize: number = Util.batchSize,
        batchDone?: () => void,
    ) {
        const res: T[] = [];
        const localBatchSize = batchSize === 0 ? total : batchSize;
        for (let i = 0; i < total; i += localBatchSize) {
            const promises = builder(localBatchSize);
            await Promise.all(promises);
            if (batchDone) {
                batchDone();
            }
        }
        return res;
    }

    static getTimestamp() {
        return Util.getMoment().format('YYYY-MM-DD_HH-mm-ss');
    }

    static getMoment(): moment.Moment {
        return moment();
    }

    static arrayChunks(array: Array<any>, chunk_size: number): Array<Array<any>> {
        return Array(Math.ceil(array.length / chunk_size))
            .fill(null)
            .map((_: any, index: number) => index * chunk_size)
            .map((begin: number) => array.slice(begin, begin + chunk_size));
    }

    static serializeSpecialTypes(data: any) {
        if (data == null) {
            return null;
        }

        const cleaned: any = {};
        Object.keys(data).map((key) => {
            let rawValue = data[key];
            if (rawValue != null && rawValue.seconds != null && rawValue.nanoseconds != null && Object.keys(rawValue).length === 2) {
                rawValue = {
                    __datatype__: 'timestamp',
                    value: {
                        _seconds: rawValue.seconds,
                        _nanoseconds: rawValue.nanoseconds,
                    },
                } as ITimestamp;
            } else if (rawValue != null && rawValue.latitude != null && rawValue.longitude != null && Object.keys(rawValue).length === 2) {
                rawValue = {
                    __datatype__: 'geopoint',
                    value: {
                        _latitude: rawValue.latitude,
                        _longitude: rawValue.longitude,
                    },
                } as IGeopoint;
            } else if (rawValue != null && rawValue.path != null && Object.keys(rawValue).length === 1) {
                rawValue = {
                    __datatype__: 'documentReference',
                    value: rawValue.path,
                } as IDocumentReference;
            } else if (rawValue === Object(rawValue)) {
                const isArray = Array.isArray(rawValue);
                rawValue = Util.serializeSpecialTypes(rawValue);
                if (isArray) {
                    rawValue = Object.keys(rawValue).map((key) => rawValue[key]);
                }
            }
            cleaned[key] = rawValue;
        });
        return cleaned;
    }

    static unserializeSpecialTypes(data: any): any {
        if (Util.isScalar(data)) {
            return data;
        } else if (Array.isArray(data)) {
            return data.map((val: any) => Util.unserializeSpecialTypes(val));
        } else if (data instanceof Object) {
            let rawValue = {...data}; // Object.assign({}, data);
            if ('__datatype__' in rawValue && 'value' in rawValue) {
                switch (rawValue.__datatype__) {
                    case 'timestamp':
                        rawValue = rawValue as ITimestamp;
                        if (rawValue.value instanceof String) {
                            const millis = Date.parse(rawValue.value);
                            rawValue = new admin.firestore.Timestamp(millis / 1000, 0);
                        } else {
                            rawValue = new admin.firestore.Timestamp(rawValue.value._seconds, rawValue.value._nanoseconds);
                        }
                        break;
                    case 'geopoint':
                        rawValue = rawValue as IGeopoint;
                        rawValue = new admin.firestore.GeoPoint(rawValue.value._latitude, rawValue.value._longitude);
                        break;
                    case 'documentReference':
                        rawValue = rawValue as IDocumentReference;
                        rawValue = admin.firestore().doc(rawValue.value);
                        break;
                }
            } else {
                const cleaned: any = {};
                Object.keys(rawValue).map((key: string) => (cleaned[key] = Util.unserializeSpecialTypes(data[key])));
                rawValue = cleaned;
            }
            return rawValue;
        }
    }

    static isScalar(val: any) {
        return (
            typeof val === 'string' ||
            val instanceof String ||
            (typeof val === 'number' && isFinite(val)) ||
            val === null ||
            typeof val === 'boolean'
        );
    }
}

export interface ComplexRef {
    isRoot: boolean;
    isDocument: boolean;
    isCol: boolean;
    ref: FirebaseFirestore.Firestore | FirebaseFirestore.DocumentReference | FirebaseFirestore.CollectionReference | any;
}
