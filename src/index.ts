#!/usr/bin/env node

import {Command} from 'commander';
import * as fs from 'fs';
import {FirestoreExport} from './firestore.export';
import {FirestoreImport} from './firestore.import';
import {FirestoreClear} from './firestore.clear';
import {FirebaseAdmin} from './firebase-admin';
import {Util} from './util';

const main = async function () {
    const cmd = new Command()
        .version('0.8.0')
        .requiredOption('-S, --serviceAccount <path>', 'Required. Location of service account JSON file.')
        .option('-E, --emulator <number>', 'Use the FireStore emulator on the provided port.')
        .option(
            '-R, --ref <path>',
            `Required for --export and --clear. Reference to collection or document. Use '${Util.ROOT}' for the root of Firestore.`,
        )
        .option('-F, --file <path>', 'Required for --export and --import. Location of file on disk.')
        .option('--export', 'Will download ref including sub-collections to file. Has priority over import and clear.')
        .option('--import', 'Will upload contents of file, merging data if required. Has priority over clear.')
        .option('--clear', 'Will delete ref, and all sub-collections.')
        .option('-X, --exclude <string>', 'Optional. Comma seperated list of collections to exclude.')
        .option('-O, --override', 'Optional. If set will override any existing file during export.')
        .option(
            '-B, --batchSize <number>',
            'Optional. Set the amount of concurrent promises fired at Firebase. Constrains memory load for constrained devices. Use 0 to disable batching.',
            '' + Util.DEFAULT_BATCH_SIZE,
        );

    cmd.on('--help', function () {
        console.log('');
        console.log('Warning:');
        console.log('  Do NOT use a formatted file for import, use the identical file syntax that is produced during export.');
        console.log('  Even though a `.json` file is exported, it is not treated like `.json` during import.');
        console.log('  The file is streamed line by line, so that it never has to be in memory completely.');
        console.log("  Formatting this file will corrupt it's structure and may lead to loss of data, or a corrupted database.");
    });

    cmd.parse(process.argv);

    const options = cmd.opts();
    if (options.serviceAccount === undefined) {
        throw Error('--serviceAccount is required');
    }
    if (options.ref === undefined && (options.export !== undefined || options.clear !== undefined || options.import === undefined)) {
        throw Error('--ref is required');
    }
    if (options.file === undefined && (options.export !== undefined || options.import !== undefined || options.clear === undefined)) {
        throw Error('--file is required');
    }
    if (options.export === undefined && options.import === undefined && options.clear === undefined) {
        throw Error('Either --export, --import, or --clear is required');
    }

    const firebaseAdmin = new FirebaseAdmin();
    firebaseAdmin.init(options.serviceAccount, options.emulator);
    Util.batchSize = Number(options.batchSize);

    if (fs.existsSync(`${process.cwd()}/${options.file}`) && !options.override) {
        throw Error(`${options.file} already exists. Use --override to force override`);
    }

    let success: boolean;
    if (options.export) {
        success = await new FirestoreExport(firebaseAdmin).execute(options.ref, options.file, undefined, options.exclude?.split(','));
    } else if (options.import) {
        success = await new FirestoreImport(firebaseAdmin).execute(options.file);
    } else if (options.clear) {
        success = await new FirestoreClear(firebaseAdmin).execute(options.ref);
    }

    console.log(success ? 'Complete successfully' : 'An error occurred');
};

if (require.main === module) {
    main()
        .then(() => {
            console.log(`Finished`);
            process.exit(0);
        })
        .catch((e) => {
            console.log(`Finished with error`, e);
            process.exit(e.code || 1);
        });
}
