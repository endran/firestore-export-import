import * as fs from 'fs';
import {Util} from './util';
import {LineReader} from './line-reader';

export class ImportService {
    constructor(
        private firestore: FirebaseFirestore.Firestore,
        private lineReader: LineReader = new LineReader(),
    ) {}

    async import(
        readStream: fs.ReadStream,
        batchSize: number = Util.batchSize,
        transformer: (data: any, path: string) => Promise<any> = async (data) => data,
    ): Promise<void> {
        let saveCount = 0;
        await this.lineReader.readLineByLine(readStream, batchSize, async (_line) => {
            const line = _line.trim();
            if (line.length > 2 && line.indexOf('"metadata"') !== 0) {
                const parse = JSON.parse(`{${line.substring(0, line.length - 1)}}`);
                const ref = Object.keys(parse)[0];
                const data = await transformer(Util.unserializeSpecialTypes(Object.values(parse)[0]), ref);

                if (data !== null) {
                    let doc: FirebaseFirestore.DocumentReference;
                    doc = this.firestore.doc(ref);
                    await doc.set(data);

                    saveCount++;
                    if (saveCount % batchSize === 0) {
                        console.log(`Saved ${saveCount} items, currently at ${ref}`);
                    }
                }
            }
            return;
        });
        console.log(`Import of ${saveCount} items complete`);
    }
}
