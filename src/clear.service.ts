import {Util} from './util';

export class ClearService {
    private paths: string[] = [];

    constructor(private firestore: FirebaseFirestore.Firestore) {}

    async clear(path: string): Promise<void> {
        const {isRoot, isDocument, isCol, ref} = Util.getRef(this.firestore, path);
        if (isRoot) {
            await this.clearCollections(ref);
        } else if (isDocument) {
            await this.clearDocument(await ref.get(), true);
        } else if (isCol) {
            await this.clearCollection(ref);
        }

        console.log(`Clear preparation complete, found ${this.paths.length} documents`);
        await Util.batchExecutor(
            this.paths.length,
            (batchSize) => {
                const start = this.paths.length - 1 - batchSize;
                return this.paths.splice(start > 0 ? start : 0, batchSize).map(async (path) => {
                    await this.firestore.doc(path).delete();
                });
            },
            undefined,
            () => console.log(`${this.paths.length} documents remaining`),
        );
    }

    private async clearDocument(snap: FirebaseFirestore.DocumentSnapshot, isStartOfProcess = false) {
        this.paths.push(snap.ref.path);

        if (isStartOfProcess) {
            await this.clearCollections(snap.ref);
        }
    }

    private async clearCollections(ref: FirebaseFirestore.DocumentReference | FirebaseFirestore.Firestore) {
        const collections = await ref.listCollections();
        for (let j = 0; j < collections.length; j++) {
            await this.clearCollection(collections[j]);
        }
    }

    private async clearCollection(ref: FirebaseFirestore.CollectionReference) {
        console.log(`Analysing collection ${ref.path}`);

        let startDoc: any = undefined;
        const nestedCollections = [];

        let snap: FirebaseFirestore.QuerySnapshot;
        while (!snap || snap.size > 0) {
            let query = ref.limit(Util.batchSize);
            if (startDoc) {
                query = query.startAfter(startDoc);
            }

            snap = await query.get();
            if (snap.size > 0) {
                startDoc = snap.docs[snap.size - 1];
            }

            await Util.batchExecutor(snap.docs.length, (batchSize) =>
                snap.docs.splice(0, batchSize).map(async (docSnapshot) => {
                    const collections = await docSnapshot.ref.listCollections();
                    if (collections.length > 0) {
                        nestedCollections.push(...collections);
                    }
                    await this.clearDocument(docSnapshot);
                }),
            );
        }

        for (let j = 0; j < nestedCollections.length; j++) {
            await this.clearCollection(nestedCollections[j]);
        }
    }
}
