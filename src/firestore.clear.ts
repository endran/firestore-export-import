import {FirebaseAdmin} from './firebase-admin';
import {ClearService} from './clear.service';

export class FirestoreClear {
    constructor(
        firebaseAdmin: FirebaseAdmin,
        private clearService = new ClearService(firebaseAdmin.firestore),
    ) {}

    async execute(ref: string): Promise<boolean> {
        try {
            await this.clearService.clear(ref);
            console.log(`Clear done`);
        } catch (e) {
            console.error('Could not clear data', e);
            return false;
        }
        return true;
    }
}
