import * as fs from 'fs';
import {Util} from './util';
import {ExportService} from './export.service';
import {FirebaseAdmin} from './firebase-admin';

export class FirestoreExport {
    constructor(
        firebaseAdmin: FirebaseAdmin,
        private exportService = new ExportService(firebaseAdmin.firestore),
    ) {}

    async execute(ref: string, file: string, timestamp = Util.getTimestamp(), excludedCollections?: string[]): Promise<boolean> {
        console.log(`Streaming data into ${process.cwd()}/${file}`);
        const writeStream = fs.createWriteStream(`${process.cwd()}/${file}`);

        try {
            await this.exportService.export(ref, writeStream, timestamp, undefined, excludedCollections);
            console.log(`Streaming done`);
        } catch (e) {
            console.error('Could not export data', e);
            return false;
        }
        writeStream.close();

        return true;
    }
}
